package in.forsk.intentexpriment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BroadcastSampleActivity extends AppCompatActivity {

    private final static String TAG = BroadcastSampleActivity.class.getSimpleName();

    Button sendBroadcastBtn;
    TextView textView;

    public final static String ACTION = "in.forsk.update_text";

    CustomBroadcastReceiver customBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_sample);

        sendBroadcastBtn = (Button) findViewById(R.id.sendBroadcastBtn);
        textView = (TextView) findViewById(R.id.textView);

        sendBroadcastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent broadcast = new Intent("in.forsk.xml_broadcast");
//                Intent broadcast = new Intent(ACTION);
                broadcast.putExtra("Data", "String received from broadcast");
                sendBroadcast(broadcast);
            }
        });

        customBroadcastReceiver = new CustomBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(ACTION);
        registerReceiver(customBroadcastReceiver, intentFilter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(customBroadcastReceiver);
    }

    public class CustomBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            Log.e(TAG, "Action Received : " + action);

            if (action.equals(ACTION)) {
                String textViewString = textView.getText().toString();
                textView.setText(textViewString + "\n" + intent.getStringExtra("Data"));
            }
        }
    }
}
