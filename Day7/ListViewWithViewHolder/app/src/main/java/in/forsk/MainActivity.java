package in.forsk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import in.forsk.adapter.FacultyListAdapter;
import in.forsk.wrapper.FacultyWrapper;

public class MainActivity extends Activity {

    private final static String TAG = MainActivity.class.getSimpleName();
    private Context context;

    ListView listView1;
    Button btnDownload;

    String raw_json = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        listView1 = (ListView) findViewById(R.id.listView1);
        btnDownload = (Button) findViewById(R.id.button1);

        btnDownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Write the code to read the json file from raw folder.

                new CustomAsyncTask().execute("http://iiitkota.forsklabs.in/faculty_profile.txt");


            }
        });
    }

    // Params, Progress, Result
    class CustomAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = readJsonFromInternet("http://iiitkota.forsklabs.in/faculty_profile.txt");
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Toast.makeText(context, "onPostExecute", Toast.LENGTH_SHORT).show();

            showJsonInListView(result);
        }

    }

    public String readJsonFromInternet(String url) {
        String response = "";
        try {
            InputStream is = Utils.openHttpConnection(url);
            response = Utils.convertStreamToString(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }


    public void showFormattedJson(String raw_json) {

        ArrayList<FacultyWrapper> facultyWrapperArrayList = Utils.pasreLocalFacultyJson(raw_json);

        StringBuilder sb = new StringBuilder();

        for (FacultyWrapper facultyWrapper : facultyWrapperArrayList) {

            sb.append("#############################");
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getFirst_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getLast_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getPhoto());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getDepartment());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getReserch_area());
            sb.append(System.getProperty("line.separator"));

            ArrayList<String> interest_areas = facultyWrapper.getInterest_areas();

            for (String s : interest_areas) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }

            ArrayList<String> contact_details = facultyWrapper.getContact_details();

            for (String s : contact_details) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }
        }

        sb.append("#############################");

//        tv.setText(sb);

    }

    public void showJsonInListView(String raw_json){
        ArrayList<FacultyWrapper> facultyWrapperArrayList = Utils.pasreLocalFacultyJson(raw_json);

        FacultyListAdapter adapter = new FacultyListAdapter(context,facultyWrapperArrayList);
        listView1.setAdapter(adapter);
    }


}
